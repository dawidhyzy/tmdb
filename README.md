# README #

Mobile coding test/interview (2 hours)
The internet movie database is a web page similar to IMDB where you can find a list of movies with their description, images, trailers and so on. It has quite interesting api to request data.
http://docs.themoviedb.apiary.io/

The goal of this coding test/interview is create a simple app which fetches the data from the API that they provides to see the latest movies, and when the user clicks on any item, see the basic details of the movie.

As special feature, the app will have a button in the main screen to filter the movies by date.

Optionally an infinite scroll will be useful to browse more movies.

Since we are talking about coding, the design does not really matter. The code structure and cleanness are the two main factors to check.

The duration of the test should be two hours.