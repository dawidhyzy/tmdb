package go.lets.yalla.tmdb.movies;

import java.util.Collections;
import java.util.List;

import go.lets.yalla.tmdb.data.api.Movie;

class TitleMovieFilter implements MovieFilter {
    @Override
    public List<Movie> filter(List<Movie> movies) {
        List<Movie> filtered = Collections.emptyList();
        for (Movie movie : movies) {
            if ("d".equalsIgnoreCase(movie.getTitle())) {
                filtered.add(movie);
            }
        }
        return filtered;
    }
}
