package go.lets.yalla.tmdb.data.api;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TmdbApi {

  @GET("movie/now_playing")
  Single<LastMoviesResult> getLastMovies(@Query("page") int page);

}
