package go.lets.yalla.tmdb.data;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import go.lets.yalla.tmdb.BuildConfig;
import go.lets.yalla.tmdb.R;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

public class NetworkModule {
    private static final long TIMEOUT = TimeUnit.SECONDS.toMillis(30);
    private Context context;

    @Nullable private HttpLoggingInterceptor httpLoggingInterceptor;
    @Nullable private OkHttpClient okHttpClient;
    @Nullable private Gson gson;
    @Nullable private Retrofit retrofit;
    @Nullable private GsonConverterFactory gsonConverterFactory;
    @Nullable private RxJava2CallAdapterFactory rxJavaCallAdapterFactory;
    @Nullable private AuthorizationInterceptor authorizationInterceptor;

    public NetworkModule(Context context) {
        this.context = context;
    }

    @NonNull
    private HttpLoggingInterceptor loggingInterceptor() {
        if (httpLoggingInterceptor == null) {
            httpLoggingInterceptor = new HttpLoggingInterceptor(Timber::d);
            httpLoggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY
                    : HttpLoggingInterceptor.Level.NONE);
        }
        return httpLoggingInterceptor;
    }

    @NonNull
    private AuthorizationInterceptor authorizationInterceptor() {
        if (authorizationInterceptor == null) {
            authorizationInterceptor = new AuthorizationInterceptor(context.getString(R.string.tmdb_api_key));
        }
        return authorizationInterceptor;
    }

    @NonNull
    private OkHttpClient okHttpClient() {
        if (okHttpClient == null) {
            okHttpClient = new OkHttpClient.Builder().connectTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
                    .readTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
                    .addNetworkInterceptor(authorizationInterceptor())
                    .addInterceptor(loggingInterceptor())
                    .build();
        }
        return okHttpClient;
    }

    @NonNull
    private Gson gson() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }

    @NonNull
    private CallAdapter.Factory callAdapterFactory() {
        if (rxJavaCallAdapterFactory == null) {
            rxJavaCallAdapterFactory = RxJava2CallAdapterFactory.create();
        }
        return rxJavaCallAdapterFactory;
    }

    @NonNull
    private Converter.Factory converterFactory() {
        if (gsonConverterFactory == null) {
            gsonConverterFactory = GsonConverterFactory.create(gson());
        }
        return gsonConverterFactory;
    }

    @NonNull
    public Retrofit retrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder().client(okHttpClient())
                    .baseUrl(context.getResources().getString(R.string.base_url))
                    .addCallAdapterFactory(callAdapterFactory())
                    .addConverterFactory(converterFactory())
                    .build();
        }
        return retrofit;
    }

}
