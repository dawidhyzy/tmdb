package go.lets.yalla.tmdb.application;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import go.lets.yalla.tmdb.data.NetworkModule;
import go.lets.yalla.tmdb.data.api.TmdbApi;

public class Injector {

    private final NetworkModule networkModule;
    @Nullable private TmdbApi tmdbApi;

    Injector(Context context) {
        networkModule = new NetworkModule(context);
    }

    @NonNull
    public TmdbApi tmdbApi() {
        if (tmdbApi == null) {
            tmdbApi = networkModule.retrofit().create(TmdbApi.class);
        }
        return tmdbApi;
    }
}
