package go.lets.yalla.tmdb.ui;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {

    private final int visibleThreshold = 6;
    // The minimum amount of items to have below your current scroll position before loading more.
    private final int FIRST_PAGE = 1;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private int previousTotal = 0;
    // The total number of items in the data-set after the last onMapReady
    private boolean loading = true;
    // True if we are still waiting for the last set of data to onMapReady.
    private int currentPage = FIRST_PAGE;

    private LinearLayoutManager linearLayoutManager;
    private OnLoadMoreListener onLoadMoreListener;

    public EndlessRecyclerOnScrollListener(LinearLayoutManager linearLayoutManager,
                                           OnLoadMoreListener onLoadMoreListener) {
        this.linearLayoutManager = linearLayoutManager;
        this.onLoadMoreListener = onLoadMoreListener;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = linearLayoutManager.getItemCount();
        firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

        if (loading && totalItemCount != previousTotal) {
            loading = false;
            previousTotal = totalItemCount;
        }

        if (!loading &&
                (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold) && hasItems()) {
            // End has been reached

            // Do something
            currentPage++;

            if (onLoadMoreListener != null) {
                onLoadMoreListener.onLoadMore(currentPage);
            }

            loading = true;
        }
    }

    private boolean hasItems() {
        return totalItemCount > 0;
    }

    public void clear() {
        previousTotal = 0;
        currentPage = FIRST_PAGE;
    }

    public void stopped() {
        loading = false;
    }

    public interface OnLoadMoreListener {
        void onLoadMore(int currentPage);
    }
}