package go.lets.yalla.tmdb.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import static java.util.Objects.requireNonNull;

public class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {

    private static final int VISIBLE_THRESHOLD = 3;
    // The minimum amount of items to have below your current scroll position before loading more.
    private static final int FIRST_PAGE = 1;
    private final LinearLayoutManager linearLayoutManager;
    private final OnLoadMoreListener onLoadMoreListener;
    private int totalItemCount;
    private int previousTotal = 0;
    // The total number of items in the data-set after the last onMapReady
    private boolean loading = true;
    // True if we are still waiting for the last set of data to onMapReady.
    private int currentPage = FIRST_PAGE;

    public EndlessRecyclerOnScrollListener(@NonNull LinearLayoutManager linearLayoutManager,
                                           @NonNull OnLoadMoreListener onLoadMoreListener) {
        this.linearLayoutManager = requireNonNull(linearLayoutManager);
        this.onLoadMoreListener = requireNonNull(onLoadMoreListener);
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        int visibleItemCount = recyclerView.getChildCount();
        totalItemCount = linearLayoutManager.getItemCount();
        int firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

        if (loading && totalItemCount != previousTotal) {
            loading = false;
            previousTotal = totalItemCount;
        }

        if (reachedThreshold(visibleItemCount, firstVisibleItem)) {
            currentPage++;
            onLoadMoreListener.onLoadMore(currentPage);
            loading = true;
        }
    }

    private boolean reachedThreshold(int visibleItemCount, int firstVisibleItem) {
        return !loading &&
                (totalItemCount - visibleItemCount) <= (firstVisibleItem + VISIBLE_THRESHOLD) && hasItems();
    }

    private boolean hasItems() {
        return totalItemCount > 0;
    }

    public void clear() {
        previousTotal = 0;
        currentPage = FIRST_PAGE;
    }

    public void stopped() {
        loading = false;
    }

    public interface OnLoadMoreListener {
        void onLoadMore(int currentPage);
    }
}