package go.lets.yalla.tmdb.ui;

import android.support.v7.app.AppCompatActivity;

import go.lets.yalla.tmdb.application.App;
import go.lets.yalla.tmdb.application.Injector;

public class BaseActivity extends AppCompatActivity {

    protected Injector getInjector() {
        return App.get(this).getInjector();
    }
}
