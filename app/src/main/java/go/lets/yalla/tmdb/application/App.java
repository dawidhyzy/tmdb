package go.lets.yalla.tmdb.application;

import android.app.Application;
import android.content.Context;

import go.lets.yalla.tmdb.BuildConfig;
import timber.log.Timber;

public class App extends Application {
    private Injector injector;

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initLogging();
        initInjector();
    }

    private void initInjector() {
        injector = new Injector(this);
    }

    private void initLogging() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    public Injector getInjector() {
        return injector;
    }
}
