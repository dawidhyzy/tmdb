package go.lets.yalla.tmdb.movies;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import go.lets.yalla.tmdb.R;
import go.lets.yalla.tmdb.data.api.Movie;
import go.lets.yalla.tmdb.databinding.MoviesBinding;
import go.lets.yalla.tmdb.movie.MovieActivity;
import go.lets.yalla.tmdb.ui.BaseActivity;
import go.lets.yalla.tmdb.ui.EndlessRecyclerOnScrollListener;
import go.lets.yalla.tmdb.ui.MovieAdapter;

public class MoviesActivity extends BaseActivity implements MoviesContract.View, MovieAdapter.OnMovieClickListener, EndlessRecyclerOnScrollListener.OnLoadMoreListener {

    private LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
    private EndlessRecyclerOnScrollListener scrollListener
            = new EndlessRecyclerOnScrollListener(linearLayoutManager, this);
    private MoviesBinding binding;
    private MoviesContract.Presenter presenter;
    private MovieAdapter adapter = new MovieAdapter(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new MoviesPresenter(this, new MoviesModel(getInjector().tmdbApi(), new TitleMovieFilter()));
        binding = DataBindingUtil.setContentView(this, R.layout.activity_movies);
        setupRecycler(binding.recycler);
        presenter.loadMovies();
    }

    private void setupRecycler(RecyclerView recycler) {
        recycler.setNestedScrollingEnabled(false);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(linearLayoutManager);
        recycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recycler.addOnScrollListener(scrollListener);
        recycler.setAdapter(adapter);
    }

    @Override
    public void onMovieClick(Movie movie) {
        presenter.onMovieClick(movie);
    }

    @Override
    public void navigateToMovie(Movie movie) {
        MovieActivity.start(this, movie);
    }

    @Override
    public void setMovies(List<Movie> movies) {
        adapter.setMovies(movies);
    }

    @Override
    public void addMovies(List<Movie> movies) {
        adapter.addMovies(movies);
    }

    @Override
    public void onLoadMore(int currentPage) {
        presenter.loadMoreMovies(currentPage);
    }
}
