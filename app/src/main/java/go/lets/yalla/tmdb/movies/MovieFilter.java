package go.lets.yalla.tmdb.movies;

import java.util.List;

import go.lets.yalla.tmdb.data.api.Movie;

public interface MovieFilter {
    List<Movie> filter(List<Movie> movies);
}
