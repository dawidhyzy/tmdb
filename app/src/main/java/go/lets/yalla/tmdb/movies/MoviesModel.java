package go.lets.yalla.tmdb.movies;

import java.util.List;

import go.lets.yalla.tmdb.data.api.LastMoviesResult;
import go.lets.yalla.tmdb.data.api.Movie;
import go.lets.yalla.tmdb.data.api.TmdbApi;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MoviesModel implements MoviesContract.Model {

    private TmdbApi api;

    MoviesModel(TmdbApi api) {
        this.api = api;
    }

    @Override
    public Single<List<Movie>> getMovies(int page) {
        return api.getLastMovies(page)
                .subscribeOn(Schedulers.io())
                .map(LastMoviesResult::getMovies)
                .observeOn(AndroidSchedulers.mainThread());
    }
}
