package go.lets.yalla.tmdb.movies;

import java.util.List;

import go.lets.yalla.tmdb.data.api.Movie;
import io.reactivex.Single;

public interface MoviesContract {

    interface Model {
        Single<List<Movie>> getMovies(int page);
    }

    interface View {

        void navigateToMovie(Movie movie);

        void setMovies(List<Movie> movies);

        void addMovies(List<Movie> movies);
    }

    interface Presenter {
        void loadMovies();
        void loadMoreMovies(int page);
        void onMovieClick(Movie movie);

    }
}
