package go.lets.yalla.tmdb.movie;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;

import go.lets.yalla.tmdb.R;
import go.lets.yalla.tmdb.data.api.Movie;
import go.lets.yalla.tmdb.databinding.MovieBinding;
import go.lets.yalla.tmdb.ui.BaseActivity;

public class MovieActivity extends BaseActivity implements MovieContract.View {
    private static final String KEY_MOVIE = "key::movie";

    private MovieBinding binding;

    public static void start(BaseActivity activity, Movie movie) {
        Intent intent = new Intent(activity, MovieActivity.class);
        intent.putExtra(KEY_MOVIE, movie);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MovieContract.Presenter presenter = new MoviePresenter(this, new MovieModel(getInjector().tmdbApi()));
        binding = DataBindingUtil.setContentView(this, R.layout.activity_movie);
        presenter.loadMovie(getIntent().getParcelableExtra(KEY_MOVIE));
    }

    @Override
    public void setMovie(Movie movie) {
        setTitle(movie.getTitle());
        binding.setMovie(movie);
    }
}
