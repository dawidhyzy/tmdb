package go.lets.yalla.tmdb.movie;

import go.lets.yalla.tmdb.data.api.Movie;

public interface MovieContract {

    interface Model {

    }

    interface View {
        void setMovie(Movie movie);
    }

    interface Presenter {
        void loadMovie(Movie movie);
    }
}
