package go.lets.yalla.tmdb.movies;

import android.support.annotation.NonNull;

import java.lang.ref.WeakReference;
import java.util.List;

import go.lets.yalla.tmdb.data.api.Movie;
import timber.log.Timber;

public class MoviesPresenter implements MoviesContract.Presenter {
    private static final int FIRST_PAGE = 1;
    private WeakReference<MoviesContract.View> view;
    private MoviesContract.Model model;
    private int page = FIRST_PAGE;

    MoviesPresenter(@NonNull MoviesContract.View view, @NonNull MoviesContract.Model model) {
        this.view = new WeakReference<>(view);
        this.model = model;
    }

    @Override
    public void loadMovies() {
        model.getMovies(page).subscribe(this::setMovies, Timber::e);
    }

    @Override
    public void loadMoreMovies(int page) {
        this.page = page;
        loadMovies();
    }

    @Override
    public void onMovieClick(Movie movie) {
        if (view.get() == null) {
            return;
        }
        view.get().navigateToMovie(movie);
    }

    private void setMovies(List<Movie> movies) {
        if (view.get() == null) {
            return;
        }
        if(page == FIRST_PAGE) {
            view.get().setMovies(movies);
        } else {
            view.get().addMovies(movies);
        }
    }
}
