package go.lets.yalla.tmdb.movie;

import java.lang.ref.WeakReference;

import go.lets.yalla.tmdb.data.api.Movie;

public class MoviePresenter implements MovieContract.Presenter {
    private WeakReference<MovieContract.View> view;
    private MovieContract.Model model;

    MoviePresenter(MovieContract.View view, MovieContract.Model model) {
        this.view = new WeakReference<>(view);
        this.model = model;
    }

    @Override
    public void loadMovie(Movie movie) {
        if (view.get() == null) {
            return;
        }
        view.get().setMovie(movie);
    }
}
