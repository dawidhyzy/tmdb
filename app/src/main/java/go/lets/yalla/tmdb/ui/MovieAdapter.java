package go.lets.yalla.tmdb.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import go.lets.yalla.tmdb.data.api.Movie;
import go.lets.yalla.tmdb.databinding.MovieItemBinding;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder>{

    @NonNull private List<Movie> movies = new ArrayList<>(0);
    private OnMovieClickListener onMovieClickListener;

    public MovieAdapter(OnMovieClickListener onMovieClickListener) {
        this.onMovieClickListener = onMovieClickListener;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MovieViewHolder(MovieItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        holder.bind(movies.get(position), onMovieClickListener);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public void setMovies(@NonNull List<Movie> movies) {
        this.movies = movies;
        notifyDataSetChanged();
    }

    public void addMovies(List<Movie> movies) {
        int previousCount = this.movies.size();
        this.movies.addAll(movies);
        notifyItemRangeInserted(previousCount, movies.size());
    }

    public interface OnMovieClickListener {
        void onMovieClick(Movie movie);
    }

    class MovieViewHolder extends RecyclerView.ViewHolder {

        private MovieItemBinding movieItemBinding;

        MovieViewHolder(MovieItemBinding movieItemBinding) {
            super(movieItemBinding.getRoot());
            this.movieItemBinding = movieItemBinding;
        }

        void bind(Movie movie, OnMovieClickListener onMovieClickListener) {
            movieItemBinding.setMovie(movie);
            movieItemBinding.setListener(onMovieClickListener);
            movieItemBinding.executePendingBindings();
        }
    }
}
